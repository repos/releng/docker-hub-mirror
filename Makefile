# This is for testing stuff before pushing to CI

.PHONY: chart setup-containerd-mirror-configuration pod-admission-controller

chart:
	docker buildx build -f .pipeline/blubber.yaml --target helm-package .

lint: lint-python

lint-python:
	docker buildx build -f .pipeline/blubber.yaml --target lint-python .

SETUP_IMAGE := localhost/docker-hub-mirror/setup-containerd-mirror-configuration

setup-containerd-mirror-configuration:
	docker buildx build -f .pipeline/blubber.yaml --target setup-containerd-mirror-configuration -t $(SETUP_IMAGE) .

POD_ADMISSION_CONTROLLER_IMAGE ?= localhost/docker-hub-mirror/pod-admission-controller

pod-admission-controller:
	docker buildx build -f .pipeline/blubber.yaml --target pod-admission-controller -t $(POD_ADMISSION_CONTROLLER_IMAGE) pod-admission-controller

push-pod-admission-controller: pod-admission-controller
	docker push $(POD_ADMISSION_CONTROLLER_IMAGE)

run-pod-admission-controller: pod-admission-controller
	docker run --rm \
		-p 5000:5000 \
		-v $(CURDIR)/pod-admission-controller/sample-config.yaml:/srv/app/config.yaml:ro \
		$(POD_ADMISSION_CONTROLLER_IMAGE)



