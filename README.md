![Docker hub mirror](logos/docker-hub-mirror-350x350.png)

This is a helm chart which deploys an instance of the [standard
container registry](https://github.com/distribution/distribution)
which has been figured to act as a read-only pull-through mirror of
the docker.io registry.
