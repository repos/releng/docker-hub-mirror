import json
import logging
import sys
import os
import base64
import yaml

import docker_image

from flask import Flask, request

app = Flask(__name__)


def read_config(filename) -> dict:
    with open(filename) as f:
        config = yaml.safe_load(f)

    if not config.get("mirror"):
        raise SystemExit(f"The configuration file {filename} must contain a key named 'mirror'")

    if not config.get("rules"):
        raise SystemExit(f"The configuration file {filename} must contain a key named 'rules'")
    if not config["rules"].get("namespaces"):
        raise SystemExit(f"The configuration file {filename} must contain a key named 'rules.namespaces'")

    return config


def safe_get(obj, *keys):
    current = obj

    for key in keys:
        if not isinstance(current, dict):
            raise SystemExit(f"""Could not index to key '{".".join(keys)}' in {obj}""")

        next = current.get(key)
        if next is None:
            return None

        current = next

    return current


def should_mutate(request_data, config) -> bool:
    op = safe_get(request_data, "request", "operation")
    logger.debug(f"request.operation is {op}")

    if op != "CREATE":
        return False

    annotations = safe_get(request_data, "request", "object", "metadata", "annotations")
    if annotations and "wikimedia.org/docker-hub-mirror" in annotations:
        logger.debug("Not mutating due to presence of wikimedia.org/docker-hub-mirror annotation")
        return False

    namespace = safe_get(request_data, "request", "namespace")

    logger.debug(f"Considering namespace '{namespace}'")

    for exclude in safe_get(config, "rules", "namespaces", "excluded"):
        if namespace == exclude:
            logger.debug(f"Namespace '{namespace}' is in the excluded list.")
            return False

    for include in safe_get(config, "rules", "namespaces", "included"):
        if namespace == include:
            logger.debug(f"Namespace '{namespace}' is in the included list.")
            return True

    logger.debug("No rules matched")
    return False


def base64_encode_string(thestring: str) -> str:
    return base64.b64encode(thestring.encode("UTF-8")).decode("UTF-8")


@app.route("/mutate", methods=["POST"])
def mutate_pod():
    logger.debug("Received admission request")
    request_data = request.get_json()
    logger.debug(f"Request data: {json.dumps(request_data)}")

    config = read_config(CONFIG_FILENAME)

    if not should_mutate(request_data, config):
        return {
            "apiVersion": "admission.k8s.io/v1",
            "kind": "AdmissionReview",
            "response": {
                "uid": request_data["request"]["uid"],
                "allowed": True,
            }
        }

    try:
        for container in request_data["request"]["object"]["spec"]["containers"]:
            image = container["image"]
            ref = docker_image.reference.Reference.parse_normalized_named(image)
            hostname, image_name = ref.split_hostname()
            if hostname.casefold() == docker_image.reference.DEFAULT_DOMAIN:
                ref["name"] = os.path.join(config["mirror"], image_name)
                logger.info(f"Replacing image {image} with {ref.string()}")
                container["image"] = ref.string()

        patch = json.dumps(
                    [
                        {
                            "op": "replace",
                            "path": "/spec/containers",
                            "value": request_data["request"]["object"]["spec"]["containers"],
                        }
                    ]
                )
    except Exception as e:
        logger.error(f"Error processing admission request: {str(e)}")
        return {
            "apiVersion": "admission.k8s.io/v1",
            "kind": "AdmissionReview",
            "response": {
                "allowed": False,
                "status": {"message": f"Error processing admission request: {str(e)}"},
            },
        }

    response_data = {
        "apiVersion": "admission.k8s.io/v1",
        "kind": "AdmissionReview",
        "response": {
            "uid": request_data["request"]["uid"],
            "allowed": True,
            "patchType": "JSONPatch",
            "patch": base64_encode_string(patch),
        },
    }

    logger.debug(f"Returning {response_data}")

    return response_data


@app.route("/healthz")
def health():
    return {"status": "ok"}


logger = logging.getLogger("admission-controller")
logger.setLevel(logging.DEBUG)
logger.addHandler(logging.StreamHandler(sys.stdout))


CONFIG_FILENAME = os.getenv("CONFIG_FILENAME", "config.yaml")

logger.info(f"Reading configuration from {CONFIG_FILENAME}")

try:
    read_config(CONFIG_FILENAME)
except Exception as e:
    raise SystemExit(f"Caught error while reading {CONFIG_FILENAME}:\n{e}\nCONFIG_FILENAME should point to the configuration file.")
