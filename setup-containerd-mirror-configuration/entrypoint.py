#!/usr/bin/env python3

import logging
import os
import shutil
import subprocess
import sys
import tomlkit

# This program must be run as root in a privileged container running
# in the host's pid namespace (e.g., hostPID: true in a kubernetes Pod
# spec, or --pid=host when running via docker)


def fail(complaint):
    raise SystemExit(complaint)


def host_path(path):
    return os.path.normpath("/proc/1/root"+path)


def log(message):
    logging.info(message)
    with open(host_path("/var/log/containerd-hacks.log"), "a") as f:
        f.write(message + "\n")


def write_file(file, contents):
    if os.path.exists(file):
        with open(file) as f:
            old_contents = f.read()
        if old_contents == contents:
            return

        backup = file + ".orig"
        if not os.path.exists(backup):
            shutil.copyfile(file, backup)

    tmp = file + ".tmp"
    with open(tmp, "w") as f:
        f.write(contents)
    os.rename(tmp, file)
    log(f"Wrote {file}")


def write_containderd_config_and_restart(config_toml, config):
    write_file(config_toml, tomlkit.dumps(config))

    log("Restarting containerd")
    cmd = ["nsenter", "--target", "1", "--mount", "--",
           "/usr/bin/systemctl", "restart", "containerd"]
    p = subprocess.run(cmd)
    if p.returncode != 0:
        fail("Failed to restart systemctl")


logging.getLogger().setLevel(logging.INFO)

mirror_url = os.getenv("MIRROR_URL")
if not mirror_url:
    fail("The MIRROR_URL environment variable is not set.")

config_toml = host_path("/etc/containerd/config.toml")

if not os.path.exists(config_toml):
    fail(f"No {config_toml} file.  Don't know what to do with this environment")

with open(config_toml) as f:
    config = tomlkit.load(f)

try:
    endpoints = config["plugins"]["io.containerd.grpc.v1.cri"]["registry"]["mirrors"]["docker.io"]["endpoint"]
except tomlkit.exceptions.NonExistentKey:
    endpoints = None

# Old way (currently used at DigitalOcean)
if endpoints:
    if mirror_url not in endpoints:
        log(f"""Adding {mirror_url} to [plugins."io.containerd.grpc.v1.cri".registry.mirrors."docker.io"].endpoint""")
        endpoints.comment(f"{mirror_url} added by docker-hub-mirror/setup-containerd-mirror-configuration")
        endpoints.insert(0, mirror_url)
        write_containderd_config_and_restart(config_toml, config)
    sys.exit(0)

# New way
certsd = "/etc/containerd/certs.d"

try:
    config_path = config["plugins"]["io.containerd.grpc.v1.cri"]["registry"]["config_path"]
except tomlkit.exceptions.NonExistentKey:
    config_path = None

if config_path != certsd:
    log(
        f"""Setting [plugins."io.containerd.grpc.v1.cri".registry].config_path to {certsd}""")
    config["plugins"]["io.containerd.grpc.v1.cri"]["registry"]["config_path"] = certsd
    config["plugins"]["io.containerd.grpc.v1.cri"]["registry"]["config_path"].comment(
        "Set by docker-hub-mirror/setup-containerd-mirror-configuration")
    write_containderd_config_and_restart(config_toml, config)

docker_io_dir = host_path(os.path.join(certsd, "docker.io"))

if not os.path.exists(docker_io_dir):
    log(f"Creating {docker_io_dir}")
    os.makedirs(docker_io_dir, 0o755)

hosts_toml = os.path.join(docker_io_dir, "hosts.toml")

hosts_toml_content = f"""# Created by docker-hub-mirror DaemonSet
# "server" specifies the default server for this registry host
# namespace. When "host(s)" are specified (after server), the hosts
# are tried first in the order listed.
server = "https://registry-1.docker.io"

# Private mirror of docker hub
[host."{mirror_url}"]
  # Resolving (the process of converting a name into a digest) must be
  # considered a trusted operation and only done by a host which is
  # trusted.
  # https://github.com/containerd/containerd/blob/main/docs/hosts.md#capabilities-field
  capabilities = ["pull", "resolve"]
"""

write_file(hosts_toml, hosts_toml_content)
